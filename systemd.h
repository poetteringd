#ifndef POETTERINGD_SYSTEMD_H
#define POETTERINGD_SYSTEMD_H

#include <glib.h>
#include <gio/gio.h>

void
systemd_on_bus_acquired (GDBusConnection *connection,
                 const gchar     *bus_name,
                 gpointer         user_data);

void
systemd_on_name_acquired (GDBusConnection *connection,
                  const gchar     *bus_name,
                  gpointer         user_data);

void
systemd_on_name_lost (GDBusConnection *connection,
              const gchar     *bus_name,
              gpointer         user_data);

void
systemd_init (gboolean read_only);

void
systemd_destroy (void);

#endif
