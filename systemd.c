#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dbus/dbus-protocol.h>
#include <glib.h>
#include <gio/gio.h>
#include <polkit/polkit.h>

#include "systemd.h"
#include "systemd1-generated.h"
#include "main.h"
#include "utils.h"

#include "rc.h"

#define QUOTE(macro) #macro
#define STR(macro) QUOTE(macro)

struct unit_invoked_name {
	GDBusMethodInvocation *invocation;
	gchar *name; /* newly allocated */
	gchar *mode; /* newly allocated */
	gint action;
};

static guint bus_id = 0;
static gboolean read_only = FALSE;

static PoetteringdSystemdSystemd1Manager *systemd1 = NULL;

G_LOCK_DEFINE_STATIC (unit);

static void
on_handle_unit_authorized_cb(GObject *source_object,
                                      GAsyncResult *res,
                                      gpointer user_data)
{
	GError *err = NULL;
	gchar **tokens = NULL;
	gchar *error_msg = NULL;
	struct unit_invoked_name *data = (struct unit_invoked_name *) user_data;
    
	G_LOCK(unit);

	if (!check_polkit_finish (res, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	}

	tokens = g_strsplit(data->name, ".", 0);
	
	if (!rc_cmd(tokens[0], RC_GET)) {
		error_msg = g_strdup_printf("Unit name %s is not valid.", tokens[0]);
		g_dbus_method_invocation_return_dbus_error (data->invocation,
						DBUS_ERROR_INVALID_ARGS,
						error_msg);
		g_free(error_msg);
		goto out;
	}

	if (!rc_cmd(tokens[0], data->action)) {
		error_msg = g_strdup_printf("Unit name %s failed for action %d.", tokens[0], data->action);
		g_dbus_method_invocation_return_dbus_error (data->invocation,
						DBUS_ERROR_INVALID_ARGS,
						error_msg);
		g_free(error_msg);
		goto out;
	}

	poetteringd_systemd_systemd1_manager_complete_start_unit (systemd1,
		data->invocation, "/org/freedesktop/systemd1/job/0");

out:
	G_UNLOCK (unit);
	g_strfreev(tokens);
	g_free (data);
	if (err != NULL)
		g_error_free (err);
}

static gboolean
on_handle_unit (PoetteringdSystemdSystemd1Manager *systemd1,
                        GDBusMethodInvocation *invocation,
                        const gchar *name,
                        const gchar *mode,
                        const gboolean user_interaction,
                        gpointer user_data)
{
	if (read_only)
		g_dbus_method_invocation_return_dbus_error (invocation,
						DBUS_ERROR_NOT_SUPPORTED,
						"poetteringd hostnamed is in read-only mode");
	else {
		const gchar *method = g_dbus_method_invocation_get_method_name(invocation);
		struct unit_invoked_name *data = g_new0 (struct unit_invoked_name, 1);

		data = g_new0 (struct unit_invoked_name, 1);
		data->invocation = invocation;
		data->name = g_strdup (name);
		data->mode = g_strdup (mode);

		if (!g_strcmp0(method, "StartUnit"))
			data->action = RC_START;
		else if (!g_strcmp0(method, "StopUnit"))
			data->action = RC_STOP;
		else if (!g_strcmp0(method, "RestartUnit"))
			data->action = RC_RESTART;
		else if (!g_strcmp0(method, "ReloadUnit"))
			data->action = RC_RELOAD;

		check_polkit_async (g_dbus_method_invocation_get_sender (invocation),
			"org.freedesktop.systemd1.manage-units", user_interaction,
			on_handle_unit_authorized_cb, data);
	}

	return TRUE;
}

static void
on_bus_acquired (GDBusConnection *connection,
                 const gchar     *bus_name,
                 gpointer         user_data)
{
	GError *err = NULL;

	g_debug ("Acquired a message bus connection");

	systemd1 = poetteringd_systemd_systemd1_manager_skeleton_new ();

	g_signal_connect (systemd1, "handle-start-unit", G_CALLBACK (on_handle_unit), NULL);
	g_signal_connect (systemd1, "handle-stop-unit", G_CALLBACK (on_handle_unit), NULL);
	g_signal_connect (systemd1, "handle-restart-unit", G_CALLBACK (on_handle_unit), NULL);
	g_signal_connect (systemd1, "handle-reload-unit", G_CALLBACK (on_handle_unit), NULL);

	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (systemd1),
						connection,
						"/org/freedesktop/systemd1",
						&err)) {
		if (err != NULL) {
			g_critical ("Failed to export interface on /org/freedesktop/systemd1: %s", err->message);
			poetteringd_exit (1);
		}
	}
}

static void
on_name_acquired (GDBusConnection *connection,
                  const gchar     *bus_name,
                  gpointer         user_data)
{
	g_debug ("Acquired the name %s", bus_name);
	poetteringd_component_started ();
}

static void
on_name_lost (GDBusConnection *connection,
              const gchar     *bus_name,
              gpointer         user_data)
{
	if (connection == NULL)
		g_critical ("Failed to acquire a dbus connection");
	else
		g_critical ("Failed to acquire dbus name %s", bus_name);

	poetteringd_exit (1);
}

/* Public functions */

void
systemd_init (gboolean _read_only)
{
	read_only = _read_only;

	bus_id = g_bus_own_name (G_BUS_TYPE_SYSTEM,
			"org.freedesktop.systemd1",
			G_BUS_NAME_OWNER_FLAGS_NONE,
			on_bus_acquired,
			on_name_acquired,
			on_name_lost,
			NULL,
			NULL);
}

void
systemd_destroy (void)
{
	g_bus_unown_name (bus_id);
	bus_id = 0;
	read_only = FALSE;
}
