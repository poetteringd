/*
  Copyright 2020 Robert Nagy

  Copyright 2012 Alexandre Rostovtsev

  Some parts are based on the code from the systemd project; these are
  copyright 2011 Lennart Poettering and others.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <dbus/dbus-protocol.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>

#include "timedated.h"
#include "timedate1-generated.h"
#include "main.h"
#include "rc.h"
#include "utils.h"

#define ZONEINFO "/usr/share/zoneinfo"
#define SERVICE_NAME "poetteringd timedated"

static guint bus_id = 0;
static gboolean read_only = FALSE;

static PoetteringdTimedatedTimedate1 *timedate1 = NULL;

static GFile *localtime_file = NULL;

/* this is loonix only */
gboolean local_rtc = FALSE;

gchar *timezone_name = NULL;
G_LOCK_DEFINE_STATIC (clock);

gboolean use_ntp = FALSE;
G_LOCK_DEFINE_STATIC (ntp);

static gchar *
get_timezone_name (GError **error)
{
	gchar *ret = NULL;
	gchar *localtime_filename = NULL;
	gchar res[PATH_MAX];

	localtime_filename = g_file_get_path (localtime_file);
	if (realpath(localtime_filename, res) == NULL) {
		g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
		    "realpath failed on %s", localtime_filename);
    		ret = g_strdup("");
		goto out;
	}

	ret = g_strdup(res + (strlen(ZONEINFO) + 1));
out:
	g_free (localtime_filename);
	return ret;
}

static gboolean
set_timezone (const gchar *_timezone_name,
              GError **error)
{
	gboolean ret = FALSE;
	gchar *tzpath;

	tzpath = g_build_filename(ZONEINFO, _timezone_name, NULL);

	g_debug("Setting timezone to %s", tzpath);

	if (g_unlink(SYSCONFDIR "/localtime") == -1)
		g_warning("Unable to remove localtime");

	if (g_file_make_symbolic_link(localtime_file, tzpath, NULL, error))
		ret = TRUE;

	return ret;
}

static const gchar *
ntp_service ()
{
	const gchar *service = "ntpd";
	return service;
}

static gboolean
service_started (const gchar *service,
                 GError **error)
{
	return rc_cmd(service, RC_CHECK);
}

static gboolean
service_disable (const gchar *service,
                 GError **error)
{
	if (!rc_cmd(service, RC_DISABLE)) {
		g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
			"unable to disable service: %s", service);
		return FALSE;
	}

	if (!rc_cmd(service, RC_STOP)) {
		g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
			"unable to stop service: %s", service);
		return FALSE;
	}

	return TRUE;
}

static gboolean
service_enable (const gchar *service,
                GError **error)
{
	if (!rc_cmd(service, RC_ENABLE)) {
		g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
			"unable to enable service: %s", service);
		return FALSE;
	}

	if (!rc_cmd(service, RC_START)) {
		g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
			"unable to start service: %s", service);
		return FALSE;
	}

	return TRUE;
}

struct invoked_set_time {
	GDBusMethodInvocation *invocation;
	gint64 usec_utc;
	gboolean relative;
};

static void
on_handle_set_time_authorized_cb (GObject *source_object,
                                  GAsyncResult *res,
                                  gpointer user_data)
{
	GError *err = NULL;
	struct invoked_set_time *data = (struct invoked_set_time *) user_data;
	struct timespec ts = { 0, 0 };

	data = (struct invoked_set_time *) user_data;
	if (!check_polkit_finish (res, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	}

	G_LOCK (clock);
	if (!data->relative && data->usec_utc < 0) {
		g_dbus_method_invocation_return_dbus_error (data->invocation, DBUS_ERROR_INVALID_ARGS, "Attempt to set time before epoch");
		goto unlock;
	}

	if (data->relative && clock_gettime (CLOCK_REALTIME, &ts)) {
		int errsv = errno;
		g_dbus_method_invocation_return_dbus_error (data->invocation, DBUS_ERROR_FAILED, strerror (errsv));
		goto unlock;
	}

	ts.tv_sec += data->usec_utc / 1000000;
	ts.tv_nsec += (data->usec_utc % 1000000) * 1000;
	if (clock_settime (CLOCK_REALTIME, &ts)) {
		int errsv = errno;
		g_dbus_method_invocation_return_dbus_error (data->invocation, DBUS_ERROR_FAILED, strerror (errsv));
		goto unlock;
	}

	poetteringd_timedated_timedate1_complete_set_time (timedate1, data->invocation);

unlock:
	G_UNLOCK (clock);

out:
	g_free (data);
	if (err != NULL)
		g_error_free (err);
}

static gboolean
on_handle_set_time (PoetteringdTimedatedTimedate1 *timedate1,
                    GDBusMethodInvocation *invocation,
                    const gint64 usec_utc,
                    const gboolean relative,
                    const gboolean user_interaction,
                    gpointer user_data)
{
	if (read_only)
		g_dbus_method_invocation_return_dbus_error (invocation,
						DBUS_ERROR_NOT_SUPPORTED,
						SERVICE_NAME " is in read-only mode");
	else {
		struct invoked_set_time *data;
		data = g_new0 (struct invoked_set_time, 1);
		data->invocation = invocation;
		data->usec_utc = usec_utc;
		data->relative = relative;
		check_polkit_async (g_dbus_method_invocation_get_sender (invocation), "org.freedesktop.timedate1.set-time", user_interaction, on_handle_set_time_authorized_cb, data);
	}

	return TRUE;
}

struct invoked_set_timezone {
	GDBusMethodInvocation *invocation;
	gchar *timezone; /* newly allocated */
};

static void
on_handle_set_timezone_authorized_cb (GObject *source_object,
                                      GAsyncResult *res,
                                      gpointer user_data)
{
	GError *err = NULL;
	struct invoked_set_timezone *data = (struct invoked_set_timezone *) user_data;

	if (!check_polkit_finish (res, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	}

	G_LOCK (clock);
	if (!set_timezone(data->timezone, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto unlock;
	}

	poetteringd_timedated_timedate1_complete_set_timezone (timedate1, data->invocation);
	g_free (timezone_name);
	timezone_name = data->timezone;
	poetteringd_timedated_timedate1_set_timezone (timedate1, timezone_name);

unlock:
	G_UNLOCK (clock);

out:
	g_free (data);
	if (err != NULL)
		g_error_free (err);
}

static gboolean
on_handle_set_timezone (PoetteringdTimedatedTimedate1 *timedate1,
                        GDBusMethodInvocation *invocation,
                        const gchar *timezone,
                        const gboolean user_interaction,
                        gpointer user_data)
{
	if (read_only)
		g_dbus_method_invocation_return_dbus_error (invocation,
						DBUS_ERROR_NOT_SUPPORTED,
						SERVICE_NAME " is in read-only mode");
	else {
		struct invoked_set_timezone *data;
		data = g_new0 (struct invoked_set_timezone, 1);
		data->invocation = invocation;
		data->timezone = g_strdup (timezone);
		check_polkit_async (g_dbus_method_invocation_get_sender (invocation), "org.freedesktop.timedate1.set-timezone", user_interaction, on_handle_set_timezone_authorized_cb, data);
	}

	return TRUE;
}

struct invoked_set_local_rtc {
	GDBusMethodInvocation *invocation;
	gboolean local_rtc;
	gboolean fix_system;
};

static gboolean
on_handle_set_local_rtc (PoetteringdTimedatedTimedate1 *timedate1,
                         GDBusMethodInvocation *invocation,
                         const gboolean _local_rtc,
                         const gboolean fix_system,
                         const gboolean user_interaction,
                         gpointer user_data)
{
	g_dbus_method_invocation_return_dbus_error (invocation,
			DBUS_ERROR_NOT_SUPPORTED,
			SERVICE_NAME " does not support local RTC");

	return TRUE;
}

struct invoked_set_ntp {
	GDBusMethodInvocation *invocation;
	gboolean use_ntp;
};

static void
on_handle_set_ntp_authorized_cb (GObject *source_object,
                                 GAsyncResult *res,
                                 gpointer user_data)
{
	GError *err = NULL;
	struct invoked_set_ntp * data = (struct invoked_set_ntp *) user_data;

	if (!check_polkit_finish (res, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	}

	G_LOCK (ntp);
	if ((data->use_ntp && !service_enable (ntp_service (), &err)) ||
	    (!data->use_ntp && !service_disable (ntp_service (), &err))) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto unlock;
	}

	poetteringd_timedated_timedate1_complete_set_ntp (timedate1, data->invocation);
	use_ntp = data->use_ntp;
	poetteringd_timedated_timedate1_set_ntp (timedate1, use_ntp);

unlock:
	G_UNLOCK (ntp);

out:
	g_free (data);
	if (err != NULL)
		g_error_free (err);
}

static gboolean
on_handle_set_ntp (PoetteringdTimedatedTimedate1 *timedate1,
                   GDBusMethodInvocation *invocation,
                   const gboolean _use_ntp,
                   const gboolean user_interaction,
                   gpointer user_data)
{
	if (read_only)
		g_dbus_method_invocation_return_dbus_error (invocation,
						DBUS_ERROR_NOT_SUPPORTED,
						SERVICE_NAME " is in read-only mode");
	else {
		struct invoked_set_ntp *data;
		data = g_new0 (struct invoked_set_ntp, 1);
		data->invocation = invocation;
		data->use_ntp = _use_ntp;
		check_polkit_async (g_dbus_method_invocation_get_sender (invocation), "org.freedesktop.timedate1.set-ntp", user_interaction, on_handle_set_ntp_authorized_cb, data);
	}

	return TRUE;
}

static void
on_bus_acquired (GDBusConnection *connection,
                 const gchar     *bus_name,
                 gpointer         user_data)
{
	GError *err = NULL;

	g_debug ("Acquired a message bus connection");

	timedate1 = poetteringd_timedated_timedate1_skeleton_new ();

	poetteringd_timedated_timedate1_set_timezone (timedate1, timezone_name);
	poetteringd_timedated_timedate1_set_local_rtc (timedate1, local_rtc);
	poetteringd_timedated_timedate1_set_ntp (timedate1, use_ntp);

	g_signal_connect (timedate1, "handle-set-time", G_CALLBACK (on_handle_set_time), NULL);
	g_signal_connect (timedate1, "handle-set-timezone", G_CALLBACK (on_handle_set_timezone), NULL);
	g_signal_connect (timedate1, "handle-set-local-rtc", G_CALLBACK (on_handle_set_local_rtc), NULL);
	g_signal_connect (timedate1, "handle-set-ntp", G_CALLBACK (on_handle_set_ntp), NULL);

	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (timedate1),
						connection,
						"/org/freedesktop/timedate1",
						&err)) {
		if (err != NULL) {
			g_critical ("Failed to export interface on /org/freedesktop/timedate1: %s", err->message);
			poetteringd_exit (1);
		}
	}
}

static void
on_name_acquired (GDBusConnection *connection,
                  const gchar     *bus_name,
                  gpointer         user_data)
{
	g_debug ("Acquired the name %s", bus_name);
	poetteringd_component_started ();
}

static void
on_name_lost (GDBusConnection *connection,
              const gchar     *bus_name,
              gpointer         user_data)
{
	if (connection == NULL)
		g_critical ("Failed to acquire a dbus connection");
	else
		g_critical ("Failed to acquire dbus name %s", bus_name);
	poetteringd_exit (1);
}

void
timedated_init (gboolean _read_only)
{
	GError *err = NULL;

	read_only = _read_only;

	localtime_file = g_file_new_for_path (SYSCONFDIR "/localtime");

	timezone_name = get_timezone_name (&err);
	if (err != NULL) {
		g_warning ("%s", err->message);
		g_clear_error (&err);
	}
	if (ntp_service () == NULL) {
		g_warning ("No ntp implementation found.");
		use_ntp = FALSE;
	} else {
		use_ntp = service_started (ntp_service (), &err);
		if (err != NULL) {
			g_warning ("%s", err->message);
			g_clear_error (&err);
		}
	}

	bus_id = g_bus_own_name (G_BUS_TYPE_SYSTEM,
			"org.freedesktop.timedate1",
			G_BUS_NAME_OWNER_FLAGS_NONE,
			on_bus_acquired,
			on_name_acquired,
			on_name_lost,
			NULL,
			NULL);
}

void
timedated_destroy (void)
{
	g_bus_unown_name (bus_id);
	bus_id = 0;
	read_only = FALSE;

	g_object_unref (localtime_file);
}
