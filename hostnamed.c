/*
  Copyright 2020 Robert Nagy

  Copyright 2012 Alexandre Rostovtsev

  Some parts are based on the code from the systemd project; these are
  copyright 2011 Lennart Poettering and others.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dbus/dbus-protocol.h>
#include <glib.h>
#include <gio/gio.h>
#include <polkit/polkit.h>

#include "hostnamed.h"
#include "hostname1-generated.h"
#include "main.h"
#include "utils.h"

#define QUOTE(macro) #macro
#define STR(macro) QUOTE(macro)

struct invoked_name {
	GDBusMethodInvocation *invocation;
	gchar *name; /* newly allocated */
};

static guint bus_id = 0;
static gboolean read_only = FALSE;

static PoetteringdHostnamedHostname1 *hostname1 = NULL;

static gchar *hostname = NULL;
G_LOCK_DEFINE_STATIC (hostname);
static gchar *static_hostname = NULL;
G_LOCK_DEFINE_STATIC (static_hostname);
static gchar *pretty_hostname = NULL;
G_LOCK_DEFINE_STATIC (pretty_hostname);
static gchar *static_hostname_filename = NULL;
static gchar *pretty_hostname_filename = NULL;
static gchar *icon_name = NULL;
G_LOCK_DEFINE_STATIC (icon_name);
static gchar *icon_name_filename = NULL;

static gboolean
hostname_is_valid (const gchar *name)
{
	if (name == NULL)
		return FALSE;

	return g_regex_match_simple ("^[a-zA-Z0-9_.-]{1," STR(HOST_NAME_MAX) "}$",
			name, G_REGEX_MULTILINE, 0);
}

static void
on_handle_set_hostname_authorized_cb(GObject *source_object,
                                      GAsyncResult *res,
                                      gpointer user_data)
{
	GError *err = NULL;
	struct invoked_name *data;
    
	data = (struct invoked_name *) user_data;
	if (!check_polkit_finish (res, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	}

	G_LOCK (hostname);
	/* Don't allow an empty or invalid hostname */
	if (!hostname_is_valid (data->name)) {
		if (data->name != NULL)
			g_free (data->name);

		if (hostname_is_valid (static_hostname))
			data->name = g_strdup (static_hostname);
		else
			data->name = g_strdup ("localhost");
	}
	if (sethostname (data->name, strlen(data->name))) {
		int errsv = errno;
		g_dbus_method_invocation_return_dbus_error (data->invocation,
					DBUS_ERROR_FAILED,
					strerror (errsv));
		G_UNLOCK (hostname);
		goto out;
	}
	g_free (hostname);
	hostname = data->name; /* data->name is g_strdup-ed already */;
	poetteringd_hostnamed_hostname1_complete_set_hostname (hostname1, data->invocation);
	poetteringd_hostnamed_hostname1_set_hostname (hostname1, hostname);
	G_UNLOCK (hostname);

out:
	g_free (data);
	if (err != NULL)
		g_error_free (err);
}

static gboolean
on_handle_set_hostname (PoetteringdHostnamedHostname1 *hostname1,
                        GDBusMethodInvocation *invocation,
                        const gchar *name,
                        const gboolean user_interaction,
                        gpointer user_data)
{
	if (read_only)
		g_dbus_method_invocation_return_dbus_error (invocation,
						DBUS_ERROR_NOT_SUPPORTED,
						"poetteringd hostnamed is in read-only mode");
	else {
		struct invoked_name *data;
		data = g_new0 (struct invoked_name, 1);
		data->invocation = invocation;
		data->name = g_strdup (name);
		check_polkit_async (g_dbus_method_invocation_get_sender (invocation),
			"org.freedesktop.hostname1.set-hostname", user_interaction,
			on_handle_set_hostname_authorized_cb, data);
	}

	return TRUE;
}

static void
on_handle_set_static_hostname_authorized_cb (GObject *source_object,
                                             GAsyncResult *res,
                                             gpointer user_data)
{
	GError *err = NULL;
	struct invoked_name *data = (struct invoked_name *) user_data;
	gchar *fixed_hostname = NULL;
	gint fnlen;

	G_LOCK (static_hostname);

	if (!check_polkit_finish (res, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	}

	/* Don't allow an empty or invalid hostname */
	if (!hostname_is_valid (data->name)) {
		if (data->name != NULL)
			g_free (data->name);

		data->name = g_strdup ("localhost");
	}

	/* myname(5) requires a newline at the end */
	fixed_hostname = g_strdup(data->name);
	fnlen = strlen(fixed_hostname);
	if (fixed_hostname[fnlen - 1] != '\n') {
		strcat(fixed_hostname, "\n");
		fnlen++;
	}

	if (!g_file_set_contents_full(static_hostname_filename, fixed_hostname, fnlen,
	    G_FILE_SET_CONTENTS_CONSISTENT, 0644, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	} 

	G_LOCK (hostname);
	if (sethostname (data->name, strlen(data->name))) {
		int errsv = errno;
		g_dbus_method_invocation_return_dbus_error (data->invocation,
					DBUS_ERROR_FAILED,
					strerror (errsv));
		G_UNLOCK (hostname);
		goto out;
	}
	g_free (hostname);
	hostname = data->name; /* data->name is g_strdup-ed already */;
	G_UNLOCK (hostname);

	g_free (static_hostname);
	static_hostname = data->name; /* data->name is g_strdup-ed already */;
	poetteringd_hostnamed_hostname1_complete_set_static_hostname (hostname1, data->invocation);
	poetteringd_hostnamed_hostname1_set_static_hostname (hostname1, static_hostname);

out:
	G_UNLOCK (static_hostname);
	g_free (fixed_hostname);
	g_free (data);
	if (err != NULL)
		g_error_free (err);
}

static gboolean
on_handle_set_static_hostname (PoetteringdHostnamedHostname1 *hostname1,
                               GDBusMethodInvocation *invocation,
                               const gchar *name,
                               const gboolean user_interaction,
                               gpointer user_data)
{
	if (read_only)
		g_dbus_method_invocation_return_dbus_error (invocation,
							DBUS_ERROR_NOT_SUPPORTED,
							"poetteringd hostnamed is in read-only mode");
	else {
		struct invoked_name *data;
		data = g_new0 (struct invoked_name, 1);
		data->invocation = invocation;
		data->name = g_strdup (name);
		check_polkit_async (g_dbus_method_invocation_get_sender (invocation), "org.freedesktop.hostname1.set-static-hostname", user_interaction, on_handle_set_static_hostname_authorized_cb, data);
	}

	return TRUE; /* Always return TRUE to indicate signal has been handled */
}

static void
on_handle_set_pretty_hostname_authorized_cb (GObject *source_object,
                                             GAsyncResult *res,
                                             gpointer user_data)
{
	GError *err = NULL;
	struct invoked_name *data = (struct invoked_name *) user_data;

    	G_LOCK (pretty_hostname);

	if (!check_polkit_finish (res, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	}

	/* Don't allow a null pretty hostname */
	if (data->name == NULL)
		data->name = g_strdup ("");

	if (!g_file_set_contents_full(pretty_hostname_filename, data->name, strlen(data->name),
	    G_FILE_SET_CONTENTS_CONSISTENT, 0644, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	} 

	g_free (pretty_hostname);
	pretty_hostname = data->name; /* data->name is g_strdup-ed already */
	poetteringd_hostnamed_hostname1_complete_set_pretty_hostname (hostname1, data->invocation);
	poetteringd_hostnamed_hostname1_set_pretty_hostname (hostname1, pretty_hostname);

out:	
	G_UNLOCK (pretty_hostname);
	g_free (data);
	if (err != NULL)
		g_error_free (err);
}

static gboolean
on_handle_set_pretty_hostname (PoetteringdHostnamedHostname1 *hostname1,
                               GDBusMethodInvocation *invocation,
                               const gchar *name,
                               const gboolean user_interaction,
                               gpointer user_data)
{
	if (read_only)
		g_dbus_method_invocation_return_dbus_error (invocation,
					DBUS_ERROR_NOT_SUPPORTED,
					"poetteringd hostnamed is in read-only mode");
	else {
		struct invoked_name *data;
		data = g_new0 (struct invoked_name, 1);
		data->invocation = invocation;
		data->name = g_strdup (name);
		check_polkit_async (g_dbus_method_invocation_get_sender (invocation),
			"org.freedesktop.hostname1.set-machine-info",
			user_interaction, on_handle_set_pretty_hostname_authorized_cb, data);
	}

	return TRUE; /* Always return TRUE to indicate signal has been handled */
}

static void
on_handle_set_icon_name_authorized_cb (GObject *source_object,
                                       GAsyncResult *res,
                                       gpointer user_data)
{
	GError *err = NULL;
	struct invoked_name *data = (struct invoked_name *) user_data;

	if (!check_polkit_finish (res, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		goto out;
	}

	G_LOCK (icon_name);

	/* Don't allow a null pretty hostname */
	if (data->name == NULL)
		data->name = g_strdup ("");

	if (!g_file_set_contents_full(icon_name_filename, data->name, strlen(data->name),
	    G_FILE_SET_CONTENTS_CONSISTENT, 0644, &err)) {
		g_dbus_method_invocation_return_gerror (data->invocation, err);
		G_UNLOCK (icon_name);
		goto out;
	} 

	g_free (icon_name);
	icon_name = data->name; /* data->name is g_strdup-ed already */
	poetteringd_hostnamed_hostname1_complete_set_icon_name (hostname1, data->invocation);
	poetteringd_hostnamed_hostname1_set_icon_name (hostname1, icon_name);

	G_UNLOCK (icon_name);

out:
	g_free (data);
	if (err != NULL)
		g_error_free (err);
}

static gboolean
on_handle_set_icon_name (PoetteringdHostnamedHostname1 *hostname1,
                         GDBusMethodInvocation *invocation,
                         const gchar *name,
                         const gboolean user_interaction,
                         gpointer user_data)
{
	if (read_only)
		g_dbus_method_invocation_return_dbus_error (invocation,
							DBUS_ERROR_NOT_SUPPORTED,
							"poetteringd hostnamed is in read-only mode");
	else {
		struct invoked_name *data;
		data = g_new0 (struct invoked_name, 1);
		data->invocation = invocation;
		data->name = g_strdup (name);
		check_polkit_async (g_dbus_method_invocation_get_sender (invocation), "org.freedesktop.hostname1.set-machine-info", user_interaction, on_handle_set_icon_name_authorized_cb, data);
	}

	return TRUE; /* Always return TRUE to indicate signal has been handled */
}

static void
on_bus_acquired (GDBusConnection *connection,
                 const gchar     *bus_name,
                 gpointer         user_data)
{
	GError *err = NULL;

	g_debug ("Acquired a message bus connection");

	hostname1 = poetteringd_hostnamed_hostname1_skeleton_new ();

	poetteringd_hostnamed_hostname1_set_hostname (hostname1, hostname);
	poetteringd_hostnamed_hostname1_set_static_hostname (hostname1, static_hostname);
	poetteringd_hostnamed_hostname1_set_pretty_hostname (hostname1, pretty_hostname);
	poetteringd_hostnamed_hostname1_set_icon_name (hostname1, icon_name);

	g_signal_connect (hostname1, "handle-set-hostname", G_CALLBACK (on_handle_set_hostname), NULL);
	g_signal_connect (hostname1, "handle-set-static-hostname", G_CALLBACK (on_handle_set_static_hostname), NULL);
	g_signal_connect (hostname1, "handle-set-pretty-hostname", G_CALLBACK (on_handle_set_pretty_hostname), NULL);
	g_signal_connect (hostname1, "handle-set-icon-name", G_CALLBACK (on_handle_set_icon_name), NULL);

	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (hostname1),
						connection,
						"/org/freedesktop/hostname1",
						&err)) {
		if (err != NULL) {
			g_critical ("Failed to export interface on /org/freedesktop/hostname1: %s", err->message);
			poetteringd_exit (1);
		}
	}
}

static void
on_name_acquired (GDBusConnection *connection,
                  const gchar     *bus_name,
                  gpointer         user_data)
{
	g_debug ("Acquired the name %s", bus_name);
	poetteringd_component_started ();
}

static void
on_name_lost (GDBusConnection *connection,
              const gchar     *bus_name,
              gpointer         user_data)
{
	if (connection == NULL)
		g_critical ("Failed to acquire a dbus connection");
	else
		g_critical ("Failed to acquire dbus name %s", bus_name);

	poetteringd_exit (1);
}

/* Public functions */

void
hostnamed_init (gboolean _read_only)
{
	gint len;
	GError *err = NULL;

	hostname = g_malloc0 (HOST_NAME_MAX + 1);
	if (gethostname (hostname, HOST_NAME_MAX)) {
		perror (NULL);
		g_strlcpy (hostname, "localhost", HOST_NAME_MAX + 1);
	}

	static_hostname_filename = g_build_filename (SYSCONFDIR "/myname", NULL);
	g_file_get_contents (static_hostname_filename, &static_hostname, NULL, &err);
	if (err != NULL) {
		g_debug ("%s", err->message);
		g_error_free (err);
		err = NULL;
	}
	len = strlen(static_hostname);
	if (len > 0 && static_hostname[len - 1] == '\n')
		static_hostname[len - 1] = '\0';

	pretty_hostname_filename = g_build_filename (SYSCONFDIR "/myprettyname", NULL);
	g_file_get_contents (pretty_hostname_filename, &pretty_hostname, NULL, &err);
	if (err != NULL) {
		g_debug ("%s", err->message);
		g_error_free (err);
		err = NULL;
		pretty_hostname = g_strdup(hostname);
	} else {
		len = strlen(pretty_hostname);
		if (len > 0 && pretty_hostname[len - 1] == '\n')
			pretty_hostname[len - 1] = '\0';
	}

	icon_name_filename = g_build_filename (SYSCONFDIR "/myiconname", NULL);
	g_file_get_contents (icon_name_filename, &icon_name, NULL, &err);
	if (err != NULL) {
		g_debug ("%s", err->message);
		g_error_free (err);
		err = NULL;
		icon_name = g_strdup("computer");
	} else {
		len = strlen(icon_name);
		if (len > 0 && icon_name[len - 1] == '\n')
			icon_name[len - 1] = '\0';
	}

	read_only = _read_only;

	bus_id = g_bus_own_name (G_BUS_TYPE_SYSTEM,
			"org.freedesktop.hostname1",
			G_BUS_NAME_OWNER_FLAGS_NONE,
			on_bus_acquired,
			on_name_acquired,
			on_name_lost,
			NULL,
			NULL);
}

void
hostnamed_destroy (void)
{
	g_bus_unown_name (bus_id);
	bus_id = 0;
	read_only = FALSE;
	g_free (hostname);
	g_free (static_hostname);
	g_free (pretty_hostname);
	g_free (icon_name);
}
