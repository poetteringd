PREFIX?=	/usr/local
BINDIR=		${PREFIX}/libexec
SHAREDIR=	${PREFIX}/share
EXAMPLEDIR=	${SHAREDIR}/examples/poetteringd
POLKITDIR=	${SHAREDIR}/polkit-1/actions
DBUSDIR=	${SHAREDIR}/dbus-1/system-services

PROG=		poetteringd
SRCS=		hostnamed.c main.c timedated.c utils.c rc.c \
		hostname1-generated.c timedate1-generated.c \
		systemd1-generated.c systemd.c

CLEANFILES+=	hostname1-generated.c \
		hostname1-generated.h \
		timedate1-generated.c \
		timedate1-generated.h \
		systemd1-generated.c \
		systemd1-generated.h
NOMAN=		1

VERSION=	0.1

CFLAGS+=	-g -Wall
CFLAGS+=	-DVERSION="\"${VERSION}\""
CFLAGS+=	-DSYSCONFDIR="\"/etc\""
CFLAGS+=	-DPIDFILE="\"/var/run/poetteringd.pid\""
CFLAGS+=	-I${.OBJDIR}
CFLAGS+=	`pkg-config --cflags glib-2.0 polkit-gobject-1 polkit-agent-1 dbus-1 gio-unix-2.0 libdaemon`

LDADD+=		`pkg-config --libs glib-2.0 polkit-gobject-1 polkit-agent-1 dbus-1 libdaemon`

GDBUS_CODEGEN=	/usr/local/bin/gdbus-codegen

INSTALL_DIR=	install -d -o root -g wheel -m 755
INSTALL_DATA=	install -c -S -o root -g wheel -m 644

hostname1-generated.h: data/org.freedesktop.hostname1.xml
	$(GDBUS_CODEGEN) \
        --interface-prefix org.freedesktop. \
        --c-namespace PoetteringdHostnamed \
        --generate-c-code hostname1-generated \
        ${.CURDIR}/data/org.freedesktop.hostname1.xml

timedate1-generated.h: data/org.freedesktop.timedate1.xml
	$(GDBUS_CODEGEN) \
        --interface-prefix org.freedesktop. \
        --c-namespace PoetteringdTimedated \
        --generate-c-code timedate1-generated \
        ${.CURDIR}/data/org.freedesktop.timedate1.xml

systemd1-generated.h: data/org.freedesktop.systemd1.xml
	$(GDBUS_CODEGEN) \
        --interface-prefix org.freedesktop. \
        --c-namespace PoetteringdSystemd \
        --generate-c-code systemd1-generated \
        ${.CURDIR}/data/org.freedesktop.systemd1.xml

hostname1-generated.c: hostname1-generated.h
timedate1-generated.c: timedate1-generated.h
systemd1-generated.c: systemd1-generated.h

hostnamed.o: hostname1-generated.h
timedated.o: timedate1-generated.h

afterinstall:
	${INSTALL_DIR} -d ${DESTDIR}${DBUSDIR} ${DESTDIR}${EXAMPLEDIR} \
		${DESTDIR}${POLKITDIR}
.for _p in hostname1 timedate1 systemd1
	${INSTALL_DATA} ${.CURDIR}/data/org.freedesktop.${_p}.policy \
		${DESTDIR}${POLKITDIR}

	sed -e 's,@libexecdir@,${BINDIR},g' ${.CURDIR}/data/org.freedesktop.${_p}.service.in > \
		${DESTDIR}${DBUSDIR}/org.freedesktop.${_p}.service
.endfor
	${INSTALL_DATA} ${.CURDIR}/data/poetteringd.conf \
		${DESTDIR}${EXAMPLEDIR}

.include <bsd.prog.mk>
