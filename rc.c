/*
 * Copyright (c) 2020 Robert Nagy <robert@openbsd.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <dbus/dbus-protocol.h>
#include <glib.h>
#include <gio/gio.h>

#include "rc.h"

gboolean
rc_cmd (const gchar *service, gint cmd)
{
	const gchar *argv[4] = { "/usr/sbin/rcctl", NULL, service, NULL };
	gint exit_status = 0;
	GError **error = NULL;

	g_assert (service != NULL);

	switch (cmd) {
	case RC_START:
		argv[1] = "start";
		break;
	case RC_STOP:
		argv[1] = "stop";
		break;
	case RC_ENABLE:
		argv[1] = "enable";
		break;
	case RC_DISABLE:
		argv[1] = "disable";
		break;
	case RC_CHECK:
		argv[1] = "check";
		break;
	case RC_GET:
		argv[1] = "get";
		break;
	case RC_RESTART:
		argv[1] = "restart";
		break;
	case RC_RELOAD:
		argv[1] = "reload";
		break;
	}

	g_debug ("Spawning rcctl with action '%s' on service '%s'", argv[1], service);

	if (!g_spawn_sync (NULL, (gchar **)argv, NULL,
	    G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL,
 	    NULL, NULL, NULL, NULL, &exit_status, error)) {
		g_prefix_error (error, "Failed to spawn %s rc service:",
			service);
		return FALSE;
	}
    
	if (!exit_status)
	        return TRUE;

	return FALSE;
}
