#define RC_START	1
#define RC_STOP		2
#define RC_ENABLE	3
#define RC_DISABLE	4
#define RC_CHECK	5
#define RC_GET		6
#define RC_RESTART	7
#define RC_RELOAD	8

#define RCD SYSCONFDIR"/rc.d"

gboolean rc_cmd (const gchar *, gint);
